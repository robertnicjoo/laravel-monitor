<?php

namespace Shizzen\Monitor;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Contracts\Bus\Dispatcher as Bus;

class DatabaseMonitor extends Model
{
    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'monitors';

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = [];

    public const JOIN_STATUS = 'joined';

    public const LEAVE_STATUS = 'left';

    public const GRACE_STATUS = 'graced';

    /**
     * Perform any actions required after the model boots.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::booted();

        static::creating(function ($monitor) {
            $monitor->id = Str::uuid();
        });

        static::saving(function ($monitor) {
            if ($monitor->status === static::GRACE_STATUS && !$monitor->graced_until) {
                $monitor->status = static::LEAVE_STATUS;
            }
        });

        static::saved(function ($monitor) {
            if ($monitor->status === static::GRACE_STATUS) {
                $job = new Jobs\LeaveChannel($monitor, $monitor->updated_at);

                app(Bus::class)->dispatch(
                    $job->delay($monitor->graced_until)
                );
            }
        });
    }

    /**
     * Get the notifiable entity that the notification belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function monitorable()
    {
        return $this->morphTo();
    }

    /**
     * Update the creation and update timestamps.
     *
     * @return void
     */
    protected function updateTimestamps()
    {
        if ($this->isDirty('status')) {
            parent::updateTimestamps();
        }
    }

    /**
     * Get the monitor's grace end.
     *
     * @return \Carbon\Carbon|null
     */
    protected function getGracedUntilAttribute()
    {
        switch ($this->status) {
            case static::GRACE_STATUS:
                $monitoredClass = Relation::getMorphedModel($this->monitorable_type) ?: $this->monitorable_type;

                if ($gracePeriod = $monitoredClass::getGracePeriod()) {
                    return $this->updated_at->addMilliseconds($gracePeriod);
                }
                else {
                    return null;
                }
                break;
            default:
                return null;
        }
    }

    /**
     * @param  string  $status
     * @return bool
     */
    public function updateStatus(string $status)
    {
        $this->status = $status;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function join()
    {
        return $this->updateStatus(static::JOIN_STATUS);
    }

    /**
     * @return bool
     */
    public function grace()
    {
        return $this->updateStatus(static::GRACE_STATUS);
    }

    /**
     * @return bool
     */
    public function leave()
    {
        return $this->updateStatus(static::LEAVE_STATUS);
    }
}
