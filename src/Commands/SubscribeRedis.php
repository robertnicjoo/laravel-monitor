<?php

namespace Shizzen\Monitor\Commands;

use Closure;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Shizzen\Monitor\Contracts\PresenceMonitor;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SubscribeRedis extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'redis:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis instance';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('default_socket_timeout', -1);

        $redisConnection = $this->laravel['redis']->connection(
            $this->option('redis')
        );

        $channels = array_map('stripslashes', $this->argument('channels'));

        $redisConnection->psubscribe(
            $channels, Closure::fromCallable([$this, 'handleSubscribe'])
        );
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['channels', InputArgument::IS_ARRAY, 'Channels to subscribe'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['redis', 'r', InputOption::VALUE_OPTIONAL, 'Redis connection to use', config('monitor.redis')],
        ];
    }

    /**
     * Handle the subscribing to a channel.
     *
     * @param  string  $message
     * @param  string  $channel
     * @return void
     */
    protected function handleSubscribe(string $message, string $channel)
    {
        switch ($channel) {
            case 'PresenceChannelUpdated':
                $event = json_decode($message, true)['event'];

                preg_match('#(?<=presence-).+(?=:members)#', $event['channel'], $channelMatch);

                $this->handleMonitor(
                    $channelMatch[0], array_column($event['members'], 'user_info')
                );
                break;
        }
    }

    /**
     * Update the presence monitoring.
     *
     * @param  string  $channel
     * @param  array  $members
     * @return void
     */
    protected function handleMonitor(string $channel, array $members)
    {
        $this->laravel->make(PresenceMonitor::class, compact('channel'))->update(new Collection($members));
    }
}
