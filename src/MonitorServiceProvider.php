<?php

namespace Shizzen\Monitor;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Config\Repository as Config;

class MonitorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/monitor.php', 'monitor'
        );

        $this->app->bind(
            Contracts\PresenceMonitor::class, $this->app[Config::class]->get('monitor.presence_monitor')
        );
    }

    /**
     * Bootstrap services.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @param  \Illuminate\Contracts\Config\Repository  $config
     * @return void
     */
    public function boot(Router $router, Config $config)
    {
        $router->middlewareGroup('presence.monitor', $config->get('monitor.middlewares'));

        $this->commands($config->get('monitor.commands'));

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config' => config_path(),
            ], 'monitor-config');

            $this->publishes([
                __DIR__.'/Commands' => app_path('Console/Commands'),
            ], 'monitor-commands');
        }
    }
}
