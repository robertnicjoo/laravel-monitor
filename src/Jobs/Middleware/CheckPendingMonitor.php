<?php

namespace Shizzen\Monitor\Jobs\Middleware;

class CheckPendingMonitor
{
    /**
     * Process the queued job.
     *
     * @param  mixed  $job
     * @param  callable  $next
     * @return mixed
     */
    public function handle($job, $next)
    {
        if ($job->date->eq($job->monitor->updated_at)) {
            $next($job);
        };
    }
}
