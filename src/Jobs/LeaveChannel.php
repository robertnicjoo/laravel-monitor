<?php

namespace Shizzen\Monitor\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Shizzen\Monitor\DatabaseMonitor;

class LeaveChannel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    /**
     * @var \Shizzen\Monitor\DatabaseMonitor
     */
    public $monitor;

    /**
     * The disconnect date
     * @var \Carbon\Carbon
     */
    public $date;

    /**
     * Create a new job instance.
     *
     * @param  \Shizzen\Monitor\DatabaseMonitor  $monitor
     * @param  \Carbon\Carbon  $date
     * @return void
     */
    public function __construct(DatabaseMonitor $monitor, Carbon $date)
    {
        $this->monitor = $monitor;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->monitor->leave();
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware()
    {
        return [new Middleware\CheckPendingMonitor];
    }
}
