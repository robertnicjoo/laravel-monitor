<?php

namespace Shizzen\Monitor;

trait Monitorable
{
    /**
     * The bootstrapper of this trait (automatically called).
     *
     * @return void
     */
    protected static function bootMonitorable()
    {
        static::deleted(function ($monitorable) {
            $monitorable->monitors()->delete();
        });
    }

    /**
     * The initializer of this trait (automatically called).
     *
     * @return void
     */
    protected function initializeMonitorable()
    {
        $this->makeHidden('monitors');
    }

    /**
     * Get the grace period (milliseconds).
     *
     * @return int
     */
    public static function getGracePeriod()
    {
        return config('monitor.grace_period');
    }

    /**
     * Get the entity's monitors.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function monitors()
    {
        return $this->morphMany(DatabaseMonitor::class, 'monitorable')->latest('updated_at');
    }
}
