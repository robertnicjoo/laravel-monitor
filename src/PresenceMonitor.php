<?php

namespace Shizzen\Monitor;

use Illuminate\Support\Collection;
use Illuminate\Notifications\AnonymousNotifiable;

class PresenceMonitor implements Contracts\PresenceMonitor
{
    /**
     * Presence channel's name.
     *
     * @var string
     */
    protected $channel;

    /**
     * Instantiate a new presence monitor.
     *
     * @param  string  $channel
     */
    public function __construct(string $channel)
    {
        $this->channel = $channel;
    }

    /**
     * Get channel's name.
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Get channel's members and optionally cast them to their monitorable entity.
     *
     * @param  bool  $cast
     * @return \Illuminate\Support\Collection
     */
    public function getMonitors(bool $cast = false)
    {
        $monitors = DatabaseMonitor::where('channel', $this->channel)->get();

        if ($cast) {
            return $monitors->load('monitorable')->pluck('monitorable');
        }

        return $monitors;
    }

    /**
     * Update monitors.
     *
     * @param  \Illuminate\Support\Collection  $members
     * @return \Illuminate\Support\Collection
     */
    public function update(Collection $members)
    {
        $channelMonitors = $this->getMonitors();

        $leavingMonitors = $channelMonitors->diffUsing($members, [$this, 'compare']);

        $leavingMonitors->each->grace();

        $joiningMonitors = $channelMonitors->diff($leavingMonitors);

        $newMembers = $members->diffUsing($joiningMonitors, function ($member, $monitor) {
            return -1 * $this->compare($monitor, $member);
        });

        foreach ($newMembers as $member) {
            $joiningMonitors->push(new DatabaseMonitor([
                'monitorable_id' => $member['id'],
                'monitorable_type' => $member['type'],
                'channel' => $this->channel,
            ]));
        }

        return $joiningMonitors->each->join();
    }

    /**
     * Compare a monitor and a member.
     *
     * @param  \Shizzen\Monitor\DatabaseMonitor  $monitor
     * @param  \ArrayAccess|array  $member
     * @return int
     */
    public function compare(DatabaseMonitor $monitor, $member)
    {
        $typeCmp = strcmp($monitor->monitorable_type, $member['type']);
        if ($typeCmp === 0) {
            return strcmp($monitor->monitorable_id, $member['id']);
        }
        return $typeCmp;
    }

    /**
     * Notify an entity with channel's members.
     *
     * @param  mixed|null  $notifiable
     * @return void
     */
    public function notify($notifiable = null)
    {
        $notifiable ??= new AnonymousNotifiable;

        $notifiable->notify(new Notifications\InitMonitoring($this));
    }
}
