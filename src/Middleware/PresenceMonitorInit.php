<?php

namespace Shizzen\Monitor\Middleware;

use Closure;
use Illuminate\Support\Str;
use Shizzen\Monitor\Contracts\PresenceMonitor;

class PresenceMonitorInit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Handle the post-request process.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $response
     * @return void
     */
    public function terminate($request, $response)
    {
        if ($response->isSuccessful()) {
            $this->handleSuccess($request, $response);
        }
    }

    /**
     * Handle the post-request process if response is successful. Returns true whether something happened.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $response
     * @return bool
     */
    protected function handleSuccess($request, $response)
    {
        $channelName = $request->input('channel_name');

        if (Str::startsWith($channelName, 'private-monitoring:')) {
            app(PresenceMonitor::class, [
                'channel' => Str::after($channelName, ':'),
            ])->notify();

            return true;
        }
        return false;
    }
}
