<?php

namespace Shizzen\Monitor\Contracts;

use Illuminate\Support\Collection;
use Shizzen\Monitor\DatabaseMonitor;

interface PresenceMonitor
{
    /**
     * Get channel's name.
     *
     * @return string
     */
    public function getChannel();

    /**
     * Get channel's members and optionally cast them to their monitorable entity.
     *
     * @param  bool  $cast
     * @return \Illuminate\Support\Collection
     */
    public function getMonitors(bool $cast = false);

    /**
     * Update monitors.
     *
     * @param  \Illuminate\Support\Collection  $members
     * @return \Illuminate\Support\Collection
     */
    public function update(Collection $members);

    /**
     * Compare a monitor and a member.
     *
     * @param  \Shizzen\Monitor\DatabaseMonitor  $monitor
     * @param  \ArrayAccess|array  $member
     * @return int
     */
    public function compare(DatabaseMonitor $monitor, $member);

    /**
     * Notify an entity with channel's members.
     *
     * @param  mixed|null  $notifiable
     * @return void
     */
    public function notify($notifiable = null);
}
