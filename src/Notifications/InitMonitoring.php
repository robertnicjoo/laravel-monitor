<?php

namespace Shizzen\Monitor\Notifications;

use Shizzen\Monitor\Contracts\PresenceMonitor;
use Illuminate\Bus\Queueable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class InitMonitoring extends Notification
{
    use Queueable;

    /**
     * Presence channel name.
     *
     * @var string
     */
    protected $channel;

    /**
     * Broadcast channel members.
     *
     * @var array
     */
    protected $members;

    /**
     * Create a new notification instance.
     *
     * @param  \Shizzen\Monitor\Contracts\PresenceMonitor  $monitor
     * @return void
     */
    public function __construct(PresenceMonitor $monitor)
    {
        $this->channel = $monitor->getChannel();
        $this->members = $monitor->getMonitors(true)->toArray();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the type of the notification being broadcast.
     *
     * @return string
     */
    public function broadcastType()
    {
        return 'init.monitoring';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('monitoring:'.$this->channel);
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage($this->toArray($notifiable));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'channel' => $this->channel,
            'members' => $this->members,
        ];
    }
}
