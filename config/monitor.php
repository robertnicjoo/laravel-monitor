<?php

return [
    'presence_monitor' => Shizzen\Monitor\PresenceMonitor::class,

    'middlewares' => [
        Shizzen\Monitor\Middleware\PresenceMonitorInit::class,
    ],

    'commands' => [
        Shizzen\Monitor\Commands\MonitorTable::class,
        Shizzen\Monitor\Commands\SubscribeRedis::class,
    ],

    'redis' => env('MONITOR_REDIS', 'default'),

    'grace_period' => env('MONITOR_GRACE_PERIOD', 0),
];
